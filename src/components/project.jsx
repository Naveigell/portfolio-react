import Image1 from "../assets/imgs/sistem-informasi-pengelolaan-jasa.png";
import Image2 from "../assets/imgs/sistem-informasi-agenda-rapat.png";
import {useLanguage} from "../context/Language";

export default function Project(){

    const { language } = useLanguage();

    return (
        <div id="projects" className="section-container pt-5">
            <div className="container">
                <div className="text-center">
                    <h2 className="p-0 m-0">
                        {
                            language.languageState.lists.last_projects
                        }
                    </h2>
                    <span className="p-0 m-0 d-inline-block separator"/>
                </div>
                <div className="row mt-5">
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div className="row mb-5" style={{margin: "10px"}}>
                            <div className="mb-1 position-relative projects-explanation">
                                <div className="position-absolute projects-overlay"/>
                                <h5 className="position-absolute d-inline-block text-white projects-title">
                                    Sistem Informasi Pengelolaan Jasa <br/> Perbaikan Alat Elektronik
                                </h5>
                                <div className="position-absolute projects-source-code-container">
                                    <div
                                        className="d-flex justify-content-center align-items-center projects-source-code-hover"
                                        style={{height: "100%"}}>
                                        <a href="https://github.com/Naveigell/sistem-informasi-pengelolaan-jasa"
                                           target="_blank" className="d-inline-block projects-source-code" rel="noreferrer">
                                            <i className="fa fa-github projects-source-code-icon"/> &nbsp; Source
                                            Code
                                        </a>
                                    </div>
                                </div>
                                <img className="projects-image"
                                     src={Image1} alt="" height="100%"
                                     width="100%"/>
                            </div>
                            <p className="fw-bold mt-5 projects-definition" dangerouslySetInnerHTML={{__html: language.languageState.lists.project_1}}/>
                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div className="row mb-5" style={{margin: "10px"}}>
                            <div className="mb-1 position-relative projects-explanation">
                                <div className="position-absolute projects-overlay"/>
                                <h5 className="position-absolute d-inline-block text-white projects-title">
                                    Sistem Informasi Agenda Rapat
                                </h5>
                                <div className="position-absolute projects-source-code-container">
                                    <div
                                        className="d-flex justify-content-center align-items-center projects-source-code-hover"
                                        style={{height: "100%"}}>
                                        <a href="https://github.com/Naveigell/sistem-informasi-agenda-rapat"
                                           target="_blank" className="d-inline-block projects-source-code" rel="noreferrer">
                                            <i className="fa fa-github projects-source-code-icon"/> &nbsp; Source
                                            Code
                                        </a>
                                    </div>
                                </div>
                                <img className="projects-image" src={Image2}
                                     alt="" height="100%" width="100%"/>
                            </div>
                            <p className="fw-bold mt-5 projects-definition" dangerouslySetInnerHTML={{__html: language.languageState.lists.project_2}}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}