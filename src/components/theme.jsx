import Moon from "./../assets/imgs/moon.png";
import Sun from "./../assets/imgs/sun.png";
import {useTheme} from "../context/Theme";
import {changeThemeToDark, changeThemeToLight} from "../reducer/Theme";
import React from "react";

export default function Theme() {
    const { theme } = useTheme();

    const handleTheme = () => {
        theme.themeDispatch(
            theme.themeState.dark ?
                changeThemeToLight() :
                changeThemeToDark()
        );
    };

    return (
        <div id="theme" className="position-fixed">
            <div onClick={() => handleTheme()} className="theme-background">
                <img src={theme.themeState.dark ? Sun : Moon} alt="" width="100%" height="100%"/>
            </div>
        </div>
    );
}