
export default function Header(){
    return (
        <div className="container mt-2">
            <nav className="navbar navbar-expand-lg navbar-light fixed-top bg-white">
                <div className="container">
                    <span className="navbar-brand portfolio-logo">Portfolio</span>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0"/>
                        <ul className="navbar-nav">
                            <li className="nav-item nav-link-hover ms-4 me-4">
                                <a className="nav-link nav-link-hover active" href="#banner">Home</a>
                            </li>
                            <li className="nav-item nav-link-hover ms-4 me-4">
                                <a className="nav-link nav-link-hover" href="#projects">My Project</a>
                            </li>
                            <li className="nav-item nav-link-hover ms-4 me-4">
                                <a className="nav-link nav-link-hover" href="#services">Services</a>
                            </li>
                            <li className="nav-item ms-4 me-4">
                                <a className="nav-link nav-link-hover" href="#skills">Skills</a>
                            </li>
                            <li className="nav-item ms-4 me-4">
                                <a className="nav-link nav-link-hover" rel="noreferrer" target="_blank" href="https://github.com/Naveigell">
                                    <i className="fa fa-github" style={{fontSize: 25, cursor: "pointer"}}/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    );
}