import MobileAppImageBlack from "../assets/imgs/mobile-app.png";
import WebsiteFrontendImageBlack from "../assets/imgs/website-frontend.png";
import WebsiteBackendImageBlack from "../assets/imgs/website-backend.png";
import MobileAppImage from "../assets/imgs/mobile-app-light.png";
import WebsiteFrontendImage from "../assets/imgs/website-frontend-light.png";
import WebsiteBackendImage from "../assets/imgs/website-backend-light.png";

import {useEffect, useMemo, useState} from "react";
import {useTheme} from "../context/Theme";
import {useLanguage} from "../context/Language";

const dark = [MobileAppImageBlack, WebsiteFrontendImageBlack, WebsiteBackendImageBlack];
const light = [MobileAppImage, WebsiteFrontendImage, WebsiteBackendImage];

export default function Services(){
    const { theme } = useTheme();
    const { language } = useLanguage();

    const [imagesState, setImagesState] = useState(light);
    const images = useMemo(() => imagesState, [imagesState]);

    useEffect(() => {
        const handleThemeChanged = () => {
            if (!theme.themeState.dark) {
                setImagesState(dark);
            } else {
                setImagesState(light);
            }
        };
        handleThemeChanged();
    }, [theme.themeState.dark]);

    return (
        <div id="services" className="pt-5 pb-5">
            <div className="text-center">
                <h2 className="p-0 m-0">
                    {
                        language.languageState.lists.service_title
                    }
                </h2>
                <span className="p-0 m-0 separator"/>
            </div>
            <div className="container mt-5">
                <div className="row">
                    <div className="col-lg-4 col-xl-4 col-md-12 col-sm-12 mb-4">
                        <div
                            className="flex-column d-flex justify-content-center align-content-center p-4 services-container">
                            <img src={images[0]} alt="" width="80px" height="80px"
                                 className="mx-auto my-1"/>
                                <span className="d-block mx-auto my-auto mt-4 services-title">
                                    Mobile App
                                </span>
                                <p className="text-center mt-4" style={{height: "100%"}} dangerouslySetInnerHTML={{__html: language.languageState.lists.service_1}}/>
                        </div>
                    </div>
                    <div className="col-lg-4 col-xl-4 col-md-12 col-sm-12 mb-4">
                        <div
                            className="flex-column d-flex justify-content-center align-content-center p-4 services-container">
                            <img src={images[1]} alt="" width="80px" height="80px"
                                 className="mx-auto my-1"/>
                                <span className="d-block mx-auto my-auto mt-4 services-title">
                                    Web Front End
                                </span>
                                <p className="text-center mt-4" style={{height: "100%"}} dangerouslySetInnerHTML={{__html: language.languageState.lists.service_2}}/>
                        </div>
                    </div>
                    <div className="col-lg-4 col-xl-4 col-md-12 col-sm-12 mb-4">
                        <div
                            className="flex-column d-flex justify-content-center align-content-center p-4 services-container">
                            <img src={images[2]} alt="" width="80px" height="80px"
                                 className="mx-auto my-1"/>
                                <span className="d-block mx-auto my-auto mt-4 services-title">
                                    Web Back End
                                </span>
                                <p className="text-center mt-4" style={{height: "100%"}} dangerouslySetInnerHTML={{__html: language.languageState.lists.service_3}}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}