import {
    DARK_THEME_MODE, LIGHT_THEME_MODE,
} from "./schema";

export const initialState = {
    dark: false
}

export const changeThemeToDark = () => ({
    type: DARK_THEME_MODE,
});

export const changeThemeToLight = () => ({
    type: LIGHT_THEME_MODE
});

export default function themeReducer(state = initialState, action){
    switch (action.type) {
        case DARK_THEME_MODE:
            return {
                dark: true
            }
        default:
            return {
                dark: false
            }
    }
}