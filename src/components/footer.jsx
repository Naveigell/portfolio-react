import React from "react";

export default function Footer(){
    return (
        <footer id="footer" className="mt-5" style={{background: "rgba(243, 244, 246, 1)", color: "#7f8083", fontSize: "15px"}}>
            <div className="pt-5 pb-3 text-center">
                <div className="d-block">
                    <h3>Let's make something great together.</h3>
                </div>
                <span className="d-block mt-4">You can contact me by email <span className="email">yosua.yerikho123@gmail.com</span></span>
                <span className="d-block mt-2"><i className="fa fa-map-marker"/>&nbsp; Tabanan, Bali, Indonesia.</span>
                <span className="d-block mt-2">Made with <img alt="❤" className="love" draggable="false"
                                                              src="https://twemoji.maxcdn.com/2/72x72/2764.png"/> by I Putu Yosua Yerikho</span>
                <span className="d-block mt-2">
                    <a href="https://github.com/Naveigell" style={{color: "#7f8083"}} target="_blank" rel="noreferrer">
                        <i className="fa fa-github" style={{fontSize: "25px", cursor: "pointer"}}/>
                    </a>
                </span>
            </div>
        </footer>
    );
}