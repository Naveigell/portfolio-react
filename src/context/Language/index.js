import React from "react";
import languageReducer, {initialState} from "../../reducer/Language";

const Language = React.createContext(null);
export const useLanguage = () => React.useContext(Language);

function LanguageProvider({ children }) {

    const [languageState, languageDispatch] = React.useReducer(languageReducer, initialState);

    return (
        <Language.Provider value={{ language: {languageState, languageDispatch} }}>
            { children }
        </Language.Provider>
    );
}

export default LanguageProvider;