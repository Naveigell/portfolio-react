import { useTheme } from "./context/Theme";
import "./assets/fonts/circular/CircularStd-Light.otf";
import "./assets/fonts/circular/CircularStd-Medium.otf";
import "./assets/fonts/circular/CircularStd-Bold.otf";
import "./assets/css/bootstrap/bootstrap.min.css";
import "./assets/css/style.css";
import "./assets/css/dark.css";
import React, { useEffect } from "react";
import Header from "./components/header";
import Banner from "./components/banner";
import Project from "./components/project";
import Services from "./components/services";
import Skills from "./components/skills";
import Footer from "./components/footer";
import Theme from "./components/theme";

import $ from "jquery";
import 'bootstrap/dist/js/bootstrap.min.js'
import Language from "./components/language";

function App() {
    const { theme } = useTheme();

    useEffect(() => {
        $('.nav-link').click(function() {
            const sectionTo = $(this).attr('href');
            const isRealLink = $(this).attr('target');

            // prevent error
            if (isRealLink)
                return;

            $('html, body').animate({
                scrollTop: $(sectionTo).offset().top - 50
            }, 300);
        });
    }, []);

    return (
        <div className={theme.themeState.dark ? 'dark-theme' : ''}>
            <Header/>
            <Banner/>
            <Project/>
            <Services/>
            <Skills/>
            <Footer/>
            <Theme/>
            <Language/>
        </div>
    );
}

export default App;
