import React from "react";
import themeReducer, {initialState} from "../../reducer/Theme";

const Theme = React.createContext(null);
export const useTheme = () => React.useContext(Theme);

function ThemeProvider({ children }) {

    const [themeState, themeDispatch] = React.useReducer(themeReducer, initialState);

    return (
        <Theme.Provider value={{ theme: {themeState, themeDispatch} }}>
            { children }
        </Theme.Provider>
    );
}

export default ThemeProvider;