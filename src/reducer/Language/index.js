import {LANGUAGE_EN, LANGUAGE_ID} from "./action";
import {languages} from "./schema";

export const initialState = {
    lists: languages.en,
    type: LANGUAGE_EN
}

export const changeLanguageToID = () => ({
    type: LANGUAGE_ID
});

export const changeLanguageToEN = () => ({
    type: LANGUAGE_EN
});

export default function languageReducer(state = initialState, action) {
    switch (action.type) {
        case LANGUAGE_EN:
            return {
                lists: languages.en,
                type: LANGUAGE_EN
            }
        default:
            return {
                lists: languages.id,
                type: LANGUAGE_ID
            }
    }
}