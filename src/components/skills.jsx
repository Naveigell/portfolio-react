import Html from "./../assets/imgs/html.png";
import Css from "./../assets/imgs/css-3.png";
import Js from "./../assets/imgs/js.png";
import Php from "./../assets/imgs/php.png";
import Java from "./../assets/imgs/java.png";
import Golang from "./../assets/imgs/golang.png";
import Python from "./../assets/imgs/python.png";
import Ruby from "./../assets/imgs/ruby.png";
import Dart from "./../assets/imgs/dart.png";
import Docker from "./../assets/imgs/docker.png";
import Mysql from "./../assets/imgs/mysql.png";
import Mongo from "./../assets/imgs/mongo-db.png";
import Bootstrap from "./../assets/imgs/bootstrap-4.png";
import Bulma from "./../assets/imgs/bulma.png";
import Node from "./../assets/imgs/node.png";
import ReactJs from "./../assets/imgs/react.png";
import Vue from "./../assets/imgs/vue-js.png";
import Angular from "./../assets/imgs/angular.png";
import Laravel from "./../assets/imgs/laravel.png";
import Codeigniter from "./../assets/imgs/codeigniter.png";
import Spring from "./../assets/imgs/spring-framework.png";
import Gin from "./../assets/imgs/gin-gonic.png";
import Django from "./../assets/imgs/django.png";
import Flask from "./../assets/imgs/flask.png";
import RubyOnRails from "./../assets/imgs/ruby-on-rails.png";
import Flutter from "./../assets/imgs/flutter.png";
import Photoshop from "./../assets/imgs/photoshop.png";
import Illustrator from "./../assets/imgs/illustrator.png";
import {useLanguage} from "../context/Language";

export default function Skills() {

    const { language } = useLanguage();

    return (
        <div id="skills" className="pt-5" style={{marginTop: "195px"}}>
            <div className="text-center">
                <h2 className="p-0 m-0">
                    {
                        language.languageState.lists.my_skills_title
                    }
                </h2>
                <span className="p-0 m-0 separator"/>
            </div>
            <div className="container mt-5">
                <div className="row skills-container">
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Html} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Css} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Js} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Php} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Java} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Golang} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Python} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Ruby} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Dart} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Docker} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Mysql} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Mongo} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Bootstrap} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Bulma} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Node} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-dark">
                                    <img src={ReactJs} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Vue} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Angular} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Laravel} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Codeigniter} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Spring} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Gin} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Django} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Flask} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={RubyOnRails} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="m-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Flutter} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row" style={{marginTop: "70px"}}>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Photoshop} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                        <div className="p-1 d-inline-block">
                            <div className="d-flex justify-content-center align-content-center">
                                <div className="p-3 skills-icon-container-light">
                                    <img src={Illustrator} alt="" width="60px" height="60px"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}