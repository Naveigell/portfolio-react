import BannerImage from "./../assets/imgs/banner.png";
import {useLanguage} from "../context/Language";
import React from "react";

export default function Banner() {

    const { language } = useLanguage();

    return (
        <div id="banner" className="section-container pt-5 pb-5 mt-5" style={{background: "transparent"}}>
            <div className="container">
                <div className="row banner-container">
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-5">
                        <h3 className="banner-sub-title">
                            {
                                language.languageState.lists.title
                            }
                        </h3>
                        <h1 className="banner-title">
                            Full Stack Developer
                        </h1>
                        <p className="banner-biography">
                            {
                                language.languageState.lists.biography
                            }
                        </p>
                        <a href="https://wa.me/+6282340800182" target="_blank" rel="noreferrer"
                           className="d-inline-block banner-contact-me">
                            {
                                language.languageState.lists.contact_me
                            }
                        </a>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-5" style={{background: "white"}}>
                        <img src={BannerImage} alt="" width="100%" height="100%"/>
                    </div>
                </div>
            </div>
        </div>
    );
}