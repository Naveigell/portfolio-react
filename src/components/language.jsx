import React from "react";
import {useLanguage} from "../context/Language";
import {LANGUAGE_ID} from "../reducer/Language/action";
import {changeLanguageToEN, changeLanguageToID} from "../reducer/Language";

export default function Language(){

    const { language } = useLanguage();
    const id = language.languageState.type === LANGUAGE_ID;

    const changeLanguage = (func) => {
        language.languageDispatch(func());
    };

    return (
        <div className="position-fixed row language-container">
            <div onClick={() => changeLanguage(changeLanguageToID)} className={`col-6 ${id ? "language-active" : ""}`}>
                Id
            </div>
            <div onClick={() => changeLanguage(changeLanguageToEN)} className={`col-6 ${!id ? "language-active" : ""}`}>
                En
            </div>
        </div>
    );
}